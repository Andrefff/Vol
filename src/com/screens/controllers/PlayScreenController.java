package com.screens.controllers;

import com.screens.Screens;
import com.screens.gameOptions.GameOptions;
import com.screens.gameOptions.Score;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by anton on 17.09.2017.
 */
public class PlayScreenController extends BaseScreenController{
    public Label nameOnePlayLabel;
    public Label scoreOnePlayLabel;
    public Label scorePlayLabel;
    public Label scoreTwoPlayLabel;
    public Label nameTwoPlayLabel;
    public Button backPlayBtn;
    public Button paysePlayBtn;
    public Button restartPlayBtn;

    public void backPlayBtnClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(Screens.MAIN_SCREEN);
    }

    public void paysePlayBtnClicked(MouseEvent mouseEvent) {
        GameOptions.getInstance().setCurrentScore(new Score(Integer.parseInt(scoreOnePlayLabel.getText()),
                Integer.parseInt(scoreTwoPlayLabel.getText())));
        GameOptions.getInstance().getScores().add(GameOptions.getInstance().getCurrentScore());
        scoreOnePlayLabel.setText(String.valueOf(GameOptions.getInstance().getCurrentScore().getPlayerOneScore()));

    }

    public void restartPlayBtn(MouseEvent mouseEvent) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
      nameOnePlayLabel.setText( GameOptions.getInstance().getFirstPlayerName());
      nameTwoPlayLabel.setText(GameOptions.getInstance().getSecondPlayerName());

    }
}
