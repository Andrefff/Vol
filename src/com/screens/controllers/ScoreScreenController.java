package com.screens.controllers;

import com.screens.Screens;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by anton on 17.09.2017.
 */
public class ScoreScreenController extends BaseScreenController {
    public Button backScoreByt;
    public Button resetScoreByt;

    public void backScoreBytClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(Screens.MAIN_SCREEN);
    }

    public void resetScoreByt(MouseEvent mouseEvent) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
