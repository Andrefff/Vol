package com.screens.controllers;

import com.screens.ScreenManager;
import com.screens.Screens;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.stage.Screen;

import java.io.IOException ;

public abstract class BaseScreenController  implements Initializable{

    protected void navigationButtonPressed(Screens screens){

        if(screens == null){
            Platform.exit();
            return;
        }
        try{
            ScreenManager.getInstance().switchScreen(screens);
        } catch (IOException e) {
            e.getMessage();
        }
    }

}
