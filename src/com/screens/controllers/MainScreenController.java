package com.screens.controllers;

import com.screens.Screens;
import com.screens.gameOptions.GameOptions;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by anton on 17.09.2017.
 */
public class MainScreenController extends BaseScreenController{
    public Button scoreMainBtn;
    public Button exitMainBtn;
    public TextField gameOneMainTF;
    public TextField gameTwoMainTF;
    public Button startMainBtn;
    public Button soundMainBtn;

    public void scoreBtnClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(Screens.SCORE_SCREEN);
    }

    public void exitBtnClicked(MouseEvent mouseEvent) {
        navigationButtonPressed(null);
    }

    public void playBtnClicked(MouseEvent mouseEvent) {
//        GameOptions.getInstance().setFirstPlayerName(gameOneMainTF.getText());
//        GameOptions.getInstance().setSecondPlayerName(gameTwoMainTF.getText());
        navigationButtonPressed(Screens.PLAY_SCREEN);
    }

    public void soundBtnClicked(MouseEvent mouseEvent) {
        if(!GameOptions.getInstance().isSoundOff()){

            soundMainBtn.setStyle(" -fx-background-image: url('images/mainScreenVeiws/music.png')");
            GameOptions.getInstance().setSoundOff(true);
        }else {soundMainBtn.setStyle(" -fx-background-image: url('images/mainScreenVeiws/m.png')");
        GameOptions.getInstance().setSoundOff(false);}

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
