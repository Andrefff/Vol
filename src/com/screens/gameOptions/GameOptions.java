package com.screens.gameOptions;

import java.util.ArrayList;
import java.util.List;

public class GameOptions {

    private String firstPlayerName;
    private String secondPlayerName;
    private Score currentScore;
    private List<Score> scores;
    private boolean isCurrentGamePause;
    private boolean soundOff;
    public static volatile GameOptions instance;

    private GameOptions(){
        firstPlayerName ="player1";
        secondPlayerName="player2";
        currentScore = new Score(1,0);
        scores = new ArrayList<>();
    }

    public static GameOptions getInstance(){
        if(instance==null){
            synchronized (GameOptions.class){
                if(instance==null){
                    instance=new GameOptions();

                }
            }
        }
        return instance;
    }

    public String getFirstPlayerName() {
        return firstPlayerName;
    }

    public void setFirstPlayerName(String firstPlayerName) {
        if(firstPlayerName ==null||firstPlayerName.length()==0){
            return;
        }
        this.firstPlayerName = firstPlayerName;
    }

    public String getSecondPlayerName() {
        return secondPlayerName;
    }

    public void setSecondPlayerName(String secondPlayerName) {
        if(secondPlayerName ==null||secondPlayerName.length()==0){
            return;
        }
        this.secondPlayerName = secondPlayerName;
    }

    public Score getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(Score currentScore) {
        this.currentScore = currentScore;
    }

    public List<Score> getScores() {
        return scores;
    }

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    public boolean isCurrentGamePause() {
        return isCurrentGamePause;
    }

    public void setCurrentGamePause(boolean currentGamePause) {
        isCurrentGamePause = currentGamePause;
    }

    public boolean isSoundOff() {
        return soundOff;
    }

    public void setSoundOff(boolean soundOff) {
        this.soundOff = soundOff;
    }



}
