package com.screens;

/**
 * Created by anton on 17.09.2017.
 */
public enum Screens {
    MAIN_SCREEN {
        @Override
        public String getStringValue() {
            return "/veiws/MainScreenVeiws.fxml";
        }
    },
    SCORE_SCREEN {
        @Override
        public String getStringValue() {
            return "/veiws/ScoreScreenVeiws.fxml";
        }
    },
    PLAY_SCREEN {
        @Override
        public String getStringValue() {
            return "/veiws/PlayScreenVeiws.fxml";
        }
    };

    public abstract String getStringValue();
}
