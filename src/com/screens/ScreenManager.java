package com.screens;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by anton on 17.09.2017.
 */
public class ScreenManager {
    private final double SCREEN_WIDTH = 1024;
    private final double SCREEN_HEIGHT = 600;
    private Stage window;
    private Map<Screens, Scene> screens = null;
    private static volatile ScreenManager instance = null;

    private ScreenManager(){
        this.screens = new HashMap<>();

    }

    public static ScreenManager getInstance(){
        if (instance == null){
            synchronized (ScreenManager.class){
                if (instance == null){
                    instance = new ScreenManager();
                }
            }
        }

        return instance;
    }

    public Stage getWindow() {
        return window;
    }

    public void setWindow(Stage window) {
        this.window = window;
        this.window.setWidth(SCREEN_WIDTH);
        this.window.setHeight(SCREEN_HEIGHT);
        this.window.setResizable(false);
        this.window.initStyle(StageStyle.DECORATED);
    }

    private Scene createScreen(Screens screen) throws IOException {
        Parent root = FXMLLoader.load(ScreenManager.class.getResource(screen.getStringValue()));
        if (root != null){
            Scene scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT, Color.BLACK);
            if(!screen.equals(Screens.PLAY_SCREEN)){
                this.screens.put(screen, scene);
            }
            return scene;
        }

        return null;
    }

    public boolean switchScreen(Screens screen) throws IOException {
        if (this.window == null){
            return false;
        }
        this.window.setScene(screens.get(screen) != null ? screens.get(screen) : createScreen(screen));
        if (this.screens.size() == 1){
            this.window.show();
        }

        return true;
    }
}


